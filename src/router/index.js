import { createRouter, createWebHistory } from 'vue-router';
import AppLayout from '@/layout/AppLayout.vue';
import Roster from '@/views/pages/roster/Roster.vue';
import AddEp from '@/views/pages/addPoints/AddEp.vue';
import AddGp from '@/views/pages/addPoints/AddGp.vue';
import Loot from '@/views/pages/loot/Loot.vue';
import Logs from '@/views/pages/logs_list/Logs.vue';
import { store } from '@/store';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: AppLayout,
            children: [
                {
                    path: '/',
                    name: 'home',
                    redirect: () => '/roster'
                },
                {
                    path: '/roster',
                    name: 'roster',
                    component: Roster,
                    meta: { auth: true }
                },
                {
                    path: '/addep',
                    name: 'Add EP',
                    component: AddEp,
                    meta: { auth: true }
                },
                {
                    path: '/addgp',
                    name: 'Add GP',
                    component: AddGp,
                    meta: { auth: true }
                },
                {
                    path: '/loot',
                    name: 'Loot',
                    component: Loot,
                    meta: { auth: true }
                },
                {
                    path: '/logs',
                    name: 'Logs',
                    component: Logs,
                    meta: { auth: true }
                }
            ]
        },
        {
            path: '/landing',
            name: 'landing',
            component: () => import('@/views/pages/Landing.vue')
        },
        {
            path: '/pages/notfound',
            name: 'notfound',
            component: () => import('@/views/pages/NotFound.vue')
        },

        {
            path: '/auth/login',
            name: 'login',
            component: () => import('@/views/pages/auth/Login.vue')
        },
        {
            path: '/auth/signup',
            name: 'signup',
            component: () => import('@/views/pages/auth/Signup.vue'),
            meta: { auth: true }
        },
        {
            path: '/auth/access',
            name: 'accessDenied',
            component: () => import('@/views/pages/auth/Access.vue')
        },
        {
            path: '/auth/error',
            name: 'error',
            component: () => import('@/views/pages/auth/Error.vue')
        },
        { path: '/:pathMatch(.*)*', redirect: () => '/notfound' }
    ]
});

router.beforeEach(async (to, from, next) => {
    let isAuth = await store.getters['auth/user'];
    if (isAuth) await store.dispatch('auth/getRio');

    if ('auth' in to.meta && to.meta.auth && !isAuth) {
        next('/landing');
    } else {
        next();
    }
});

export default router;
