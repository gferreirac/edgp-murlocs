import { collection, onSnapshot } from 'firebase/firestore';
import { db } from '@/firebase';

export default {
    namespaced: true,

    state: {
        ep_types: null
    },
    actions: {
        async get(context) {
            try {
                onSnapshot(collection(db, 'ep_types'), (querySnapshot) => {
                    context.commit('set', querySnapshot);
                });
            } catch (error) {
                throw new Error(error);
            }
        }
    },
    mutations: {
        set(state, payload) {
            let fb = [];
            payload.forEach((doc) => {
                const data = doc.data();
                data.id = doc.id;
                fb.push(data);
            });
            state.ep_types = fb;
        }
    },
    getters: {
        ep_types(state) {
            return state.ep_types;
        }
    }
};
