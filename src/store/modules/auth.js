import { auth, db } from '@/firebase';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from 'firebase/auth';
import { collection, query, where, getDocs } from 'firebase/firestore';
import axios from 'axios';

export default {
    namespaced: true,

    state: {
        user: null,
        rio: {
            name: '',
            race: '',
            class: '',
            active_spec_name: '',
            active_spec_role: '',
            faction: '',
            thumbnail_url: '',
            realm: '',
            profile_url: '',
            rio: '',
            ilvl: '',
            raid_progress: ''
        }
    },
    actions: {
        async signup(context, { email, password }) {
            await createUserWithEmailAndPassword(auth, email, password)
                .then(() => {
                    console.log('Officer criado com sucesso');
                })
                .catch((error) => {
                    throw new Error(error);
                });
        },

        async login(context, { email, password }) {
            await signInWithEmailAndPassword(auth, email, password)
                .then(async (res) => {
                    context.commit('setUser', res.user);
                })
                .catch((error) => {
                    throw new Error(error);
                });
        },

        async logout(context) {
            await signOut(auth);
            context.commit('signOut');
        },

        async getRio(context) {
            const querySnapshot = await getDocs(query(collection(db, 'roster'), where('email', '==', context.state.user.email)));
            querySnapshot.forEach(async (doc) => {
                const params = {
                    region: 'us',
                    realm: doc.data().server,
                    name: doc.data().name,
                    fields: 'gear,raid_progression,mythic_plus_scores_by_season:current'
                };
                return await axios
                    .get('https://raider.io/api/v1/characters/profile', { params })
                    .then((response) => {
                        context.commit('setRio', response.data);
                    })
                    .catch((error) => {
                        throw new Error(error);
                    });
            });
        }
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload;
        },
        setRio(state, payload) {
            // console.log(payload);
            state.rio = {
                name: payload.name,
                race: payload.race,
                class: payload.class,
                active_spec_name: payload.active_spec_name,
                active_spec_role: payload.active_spec_role,
                faction: payload.faction,
                thumbnail_url: payload.thumbnail_url,
                realm: payload.realm,
                profile_url: payload.profile_url,
                rio: payload.mythic_plus_scores_by_season[0].scores.all,
                ilvl: payload.gear.item_level_equipped,
                raid_progress: payload.raid_progression
            };
        },
        signOut(state) {
            state.user = null;
        }
    },
    getters: {
        user(state) {
            return state.user;
        },

        rio(state) {
            return state.rio;
        }
    }
};
