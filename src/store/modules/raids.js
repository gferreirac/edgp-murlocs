import { collection, onSnapshot, addDoc, deleteDoc, doc } from 'firebase/firestore';
import { db } from '@/firebase';

export default {
    namespaced: true,

    state: {
        raids: null,
        currentRaid: '10.1'
    },
    actions: {
        async get(context) {
            try {
                onSnapshot(collection(db, 'raid'), (querySnapshot) => {
                    context.commit('set', querySnapshot);
                });
            } catch (error) {
                throw new Error(error);
            }
        },

        async create(context, form) {
            try {
                console.log('create', form);
                await addDoc(collection(db, 'raid'), form);
            } catch (error) {
                throw new Error(error);
            }
        },

        async delete(context, id) {
            try {
                await deleteDoc(doc(collection(db, 'raid'), id));
            } catch (error) {
                throw new Error(error);
            }
        }
    },
    mutations: {
        set(state, payload) {
            let fb = [];
            payload.forEach((doc) => {
                const data = doc.data();
                data.id = doc.id;
                data.pr = doc.data().ep / doc.data().gp;
                fb.push(data);
            });
            state.raids = fb;
        }
    },
    getters: {
        raids(state) {
            return state.raids;
        },
        currentRaid(state) {
            return state.currentRaid;
        }
    }
};
