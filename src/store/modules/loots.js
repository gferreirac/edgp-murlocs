import { addDoc, collection, deleteDoc, doc, getDocs, query, updateDoc, where } from 'firebase/firestore';
import { db } from '@/firebase';

export default {
    namespaced: true,

    state: {
        loots: null,
        lootsByPatch: null
    },
    actions: {
        async get(context, id) {
            try {
                const querySnapshot = await getDocs(query(collection(db, 'raid', id, 'loot')));
                context.commit('set', querySnapshot);
            } catch (error) {
                throw new Error(error);
            }
        },

        async getByPatch(context, patch) {
            try {
                let raid_id = '';
                const raid = await getDocs(query(collection(db, 'raid'), where('patch', '==', patch)));
                raid.forEach(async (doc) => {
                    console.log(doc.data());
                    raid_id = doc.id;
                });
                const querySnapshot = await getDocs(query(collection(db, 'raid', raid_id, 'loot')));
                context.commit('setByPatch', querySnapshot);
            } catch (error) {
                throw new Error(error);
            }
        },

        async create(context, { form, idRaid }) {
            try {
                console.log('create', form);
                await addDoc(collection(db, 'raid', idRaid, 'loot'), form);
            } catch (error) {
                throw new Error(error);
            }
        },

        async update(context, { form, idRaid }) {
            try {
                console.log(form, idRaid);
                const data = {
                    boss: form.boss,
                    icon: form.icon,
                    name: form.name,
                    name_ptbr: form.name_ptbr,
                    slot: form.slot,
                    url: form.url
                };
                await updateDoc(doc(collection(db, 'raid', idRaid, 'loot'), form.id), data);
            } catch (error) {
                throw new Error(error);
            }
        },

        async delete(context, { idLoot, idRaid }) {
            try {
                await deleteDoc(doc(collection(db, 'raid', idRaid, 'loot'), idLoot));
            } catch (error) {
                throw new Error(error);
            }
        }
    },
    mutations: {
        set(state, payload) {
            let fb = [];
            payload.forEach(async (doc) => {
                const data = doc.data();
                data.id = doc.id;
                fb.push(data);
            });
            state.loots = fb;
        },
        setByPatch(state, payload) {
            let fb = [];
            payload.forEach(async (doc) => {
                const data = doc.data();
                data.id = doc.id;
                fb.push(data);
            });
            state.lootsByPatch = fb;
        }
    },
    getters: {
        loots(state) {
            return state.loots;
        },
        lootsByPatch(state) {
            return state.lootsByPatch;
        }
    }
};
