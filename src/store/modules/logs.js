import { collection, onSnapshot, addDoc, deleteDoc, doc, query, orderBy } from 'firebase/firestore';
import { db } from '@/firebase';

export default {
    namespaced: true,

    state: {
        logs: null
    },
    actions: {
        async get(context) {
            try {
                const q = query(collection(db, 'logs'), orderBy('date', 'desc'));
                onSnapshot(q, (querySnapshot) => {
                    console.log(querySnapshot);
                    context.commit('set', querySnapshot);
                });
            } catch (error) {
                throw new Error(error);
            }
        },

        async create(context, form) {
            try {
                await addDoc(collection(db, 'logs'), form);
            } catch (error) {
                throw new Error(error);
            }
        },

        async delete(context, id) {
            try {
                await deleteDoc(doc(collection(db, 'logs'), id));
            } catch (error) {
                throw new Error(error);
            }
        }
    },
    mutations: {
        set(state, payload) {
            state.logs = null;
            let fb = [];
            payload.forEach((doc) => {
                const data = doc.data();
                data.id = doc.id;
                data.pr = doc.data().ep / doc.data().gp;
                fb.push(data);
            });
            state.logs = fb;
        }
    },
    getters: {
        logs(state) {
            return state.logs;
        }
    }
};
