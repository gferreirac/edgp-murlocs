import { collection, onSnapshot } from 'firebase/firestore';
import { db } from '@/firebase';

export default {
    namespaced: true,

    state: {
        classes: null
    },
    actions: {
        async get(context) {
            try {
                onSnapshot(collection(db, 'classes'), (querySnapshot) => {
                    context.commit('set', querySnapshot);
                });
            } catch (error) {
                throw new Error(error);
            }
        }
    },
    mutations: {
        set(state, payload) {
            let fb = [];
            payload.forEach((doc) => {
                const data = doc.data();
                data.id = doc.id;
                fb.push(data);
            });
            state.classes = fb;
        }
    },
    getters: {
        classes(state) {
            return state.classes;
        }
    }
};
