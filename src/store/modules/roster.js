import { collection, onSnapshot, addDoc, deleteDoc, doc, updateDoc, getDoc } from 'firebase/firestore';
import { db } from '@/firebase';

export default {
    namespaced: true,

    state: {
        roster: null
    },
    actions: {
        async getRoster(context) {
            try {
                onSnapshot(collection(db, 'roster'), (querySnapshot) => {
                    context.commit('setRoster', querySnapshot);
                });
            } catch (error) {
                throw new Error(error);
            }
        },

        async find(context, id) {
            try {
                const member = await getDoc(doc(db, 'roster', id));
                if (member.exists()) {
                    const data = member.data();
                    data.id = member.id;
                    return data;
                } else {
                    throw new Error('Nenhum membro encontrado');
                }
            } catch (error) {
                throw new Error(error);
            }
        },

        async create(context, form) {
            try {
                console.log('create', form);
                await addDoc(collection(db, 'roster'), form);
            } catch (error) {
                throw new Error(error);
            }
        },

        async update(context, form) {
            try {
                const data = {
                    class: form.class,
                    email: form.email,
                    ep: form.ep,
                    gp: form.gp,
                    name: form.name,
                    role: form.role,
                    server: form.server
                };

                await updateDoc(doc(collection(db, 'roster'), form.id), data);
            } catch (error) {
                throw new Error(error);
            }
        },

        async delete(context, id) {
            try {
                await deleteDoc(doc(collection(db, 'roster'), id));
            } catch (error) {
                throw new Error(error);
            }
        }
    },
    mutations: {
        setRoster(state, payload) {
            let fbRoster = [];
            payload.forEach((doc) => {
                const data = doc.data();
                data.id = doc.id;
                data.pr = doc.data().ep / doc.data().gp;
                fbRoster.push(data);
            });
            state.roster = fbRoster;
        }
    },
    getters: {
        roster(state) {
            return state.roster;
        }
    }
};
