import { collection, getDocs, query } from 'firebase/firestore';
import { db } from '@/firebase';

export default {
    namespaced: true,

    state: {
        bosses: null
    },
    actions: {
        async get(context, id) {
            try {
                const querySnapshot = await getDocs(query(collection(db, 'raid', id, 'bosses')));
                context.commit('set', querySnapshot);
            } catch (error) {
                throw new Error(error);
            }
        }
    },
    mutations: {
        set(state, payload) {
            let fb = [];
            payload.forEach((doc) => {
                const data = doc.data();
                data.id = doc.id;
                fb.push(data);
            });
            state.bosses = fb;
        }
    },
    getters: {
        bosses(state) {
            return state.bosses;
        }
    }
};
