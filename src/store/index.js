import { createStore } from 'vuex';
// import api from '../service/api';
import { useCookies } from 'vue3-cookies';
const { cookies } = useCookies();

import auth from './modules/auth';
import roster from './modules/roster';
import classes from './modules/classes';
import roles from './modules/roles';
import ep_types from './modules/ep_types';
import logs from './modules/logs';
import raids from './modules/raids';
import slots from './modules/slots';
import bosses from './modules/bosses';
import loots from './modules/loots';

import VuexPersistence from 'vuex-persist';
const vuexLocal = new VuexPersistence({
    storage: window.localStorage
});
const vuexCookie = new VuexPersistence({
    restoreState: (key) => cookies.get(key),
    saveState: (key, state) =>
        cookies.set(key, state, {
            expires: 3
        })
});

export const store = createStore({
    plugins: [vuexCookie.plugin, vuexLocal.plugin],

    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        auth,
        roster,
        classes,
        roles,
        ep_types,
        logs,
        raids,
        slots,
        bosses,
        loots
    }
});
